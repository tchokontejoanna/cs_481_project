﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace finale_project
{
    public class Rest : IRest
    {
        private HttpClient _client;

        public Rest()
        {
            _client = new HttpClient();
        }

        protected override async Task<T> get<T>(string url)
        {
            var uri = new Uri(string.Format(url, string.Empty));
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var tmp_result = await response.Content.ReadAsStringAsync();
                Console.WriteLine("-----");
                Console.WriteLine(tmp_result);
                Console.WriteLine("-----");
                return JsonConvert.DeserializeObject<T>(tmp_result);
            }
            return default(T);
        }
    }
}
