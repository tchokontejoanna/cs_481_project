﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    public partial class ContactPage : ContentPage
    {
        int i = 4;
        public ContactPage()
        {
            InitializeComponent();
            Title = "Feedback";
            var image = new Image { Source = "france.png" };
            image.Source = Device.RuntimePlatform == Device.Android
                            ? ImageSource.FromFile("france.png")
                            : ImageSource.FromFile("Images/france.png");

        }


        public void Handle_Appearing(object sender, System.EventArgs e)
        {
            DisplayAlert("Thank you", "Your feedback has been sent correctly", "Got it ");
        }


    }
}