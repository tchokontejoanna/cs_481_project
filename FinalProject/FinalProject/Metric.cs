﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class Metric
    {
        public long amount { get; set; }
        public string unitLong { get; set; }
        public string unitShort { get; set; }

    }
}
