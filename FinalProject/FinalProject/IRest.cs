﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace finale_project
{
    public abstract class IRest
    {
        protected abstract Task<T> get<T>(string url);
    }
}
