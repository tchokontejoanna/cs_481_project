﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class ProductMatch
    {
        public long id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string imageUrl { get; set; }
        public float averageRating { get; set; }
        public long ratingCount { get; set; }
        public float score { get; set; }
        public string link { get; set; }

    }
}
