﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class SearchRecipe
    {
        public List<Recipe> results { get; set; }
        public string baseUri { get; set; }
        public long offset { get; set; }
        public long number { get; set; }
        public long totalResults { get; set; }
        public long processingTimeMs { get; set; }
        public long expires { get; set; }
        public bool isStale { get; set; }
    }
}
