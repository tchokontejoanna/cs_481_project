﻿using finale_project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeDay : ContentPage
    {

        Spoonacular _spoonacular;
        public IList<Recipe> recipes { get; private set; }
        public RecipeDay()
        {
            InitializeComponent();

            BindingContext = this;
            Init();
        }

        private async void Init()
        {
            _spoonacular = new Spoonacular();
            await _spoonacular.search();
            recipes = _spoonacular.recipes;

            RecipeInfo recipeInfo = await _spoonacular.get_recipe_info(recipes[7].id);



            // Create Recipe of the Day
            mainPage.Children.Add(new Label { Text = "Recipe of the Day", TextColor = Color.FromHex("#ff8a0d"), FontSize = 40, FontAttributes = FontAttributes.Bold, Padding = 10 });
            mainPage.Children.Add(new Image { Source = "https://webknox.com/recipeImages/" + recipes[7].image });
            mainPage.Children.Add(new Label { Text = recipes[7].title, FontSize = 20, FontAttributes = FontAttributes.Bold, Padding = 10 });
            mainPage.Children.Add(new Label { Text = recipeInfo.instructions, FontSize = 20, Padding = 15 });
            mainPage.Children.Add(new Label { Text = "Ready in " + recipes[7].readyInMinutes + " minutes\n" + recipes[7].servings + " plate(s)", FontSize = 15, FontAttributes = FontAttributes.Italic, Padding = 15 });
        }

        void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Recipe selectedItem = e.SelectedItem as Recipe;
        }

        void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            Recipe tappedItem = e.Item as Recipe;
        }
    }
}