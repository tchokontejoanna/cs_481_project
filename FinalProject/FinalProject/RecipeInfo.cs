﻿using FinalProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class RecipeInfo
    {
        public int id { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string imageType { get; set; }
        public long servings { get; set; }
        public long readyInMinutes { get; set; }
        public string license { get; set; }
        public string sourceName { get; set; }
        public string sourceUrl { get; set; }
        public string spoonacularSourceUrl { get; set; }
        public long aggregateLikes { get; set; }
        public long healthScore { get; set; }
        public long spoonacularScore { get; set; }
        public float pricePerServing { get; set; }
        public List<Item> analyzedInstructions { get; set; }
        public bool cheap { get; set; }
        public string creditsText { get; set; }
        public List<string> cuisines { get; set; }
        public bool dairyFree { get; set; }
        public List<string> diets { get; set; }
        public string gaps { get; set; }
        public bool glutenFree { get; set; }
        public string instructions { get; set; }
        public bool ketogenic { get; set; }
        public bool lowFodmap { get; set; }
        public List<string> occasions { get; set; }
        public bool sustainable { get; set; }
        public bool vegan { get; set; }
        public bool vegetarian { get; set; }
        public bool veryHealthy { get; set; }
        public bool veryPopular { get; set; }
        public bool whole30 { get; set; }
        public long weightWatcherSmartPoints { get; set; }
        public List<string> dishTypes { get; set; }
        public List<ExtendedIngredients> extendedIngredients { get; set; }
        public string summary { get; set; }
        public WinePairing winePairing { get; set; }

    }
}
