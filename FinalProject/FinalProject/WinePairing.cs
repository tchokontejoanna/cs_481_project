﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class WinePairing
    {
        public List<string> pairedWines { get; set; }
        public string pairingText { get; set; }
        public List<ProductMatch> productMatches { get; set; }

    }
}
