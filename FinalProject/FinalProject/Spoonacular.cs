﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace finale_project
{
    public class Spoonacular : Rest
    {
        private string _base_uri = "https://api.spoonacular.com/";
        private string _api_key = "4ba5a05abed94a87b66f451556788127";

        public List<Recipe> recipes { get; set; }


        public async Task<SearchRecipe> search(string query_params = "number=8")
        {
            SearchRecipe sr = await this.get<SearchRecipe>($"{this._base_uri}recipes/search?apiKey={this._api_key}&{query_params}");
            this.recipes = sr.results;
            return sr;
        }

        public async Task<RecipeInfo> get_recipe_info(long id, string query_params = "number=8")
        {
            return await this.get<RecipeInfo>($"{this._base_uri}recipes/{id}/information?apiKey={this._api_key}&{query_params}");
        }
    }
}
