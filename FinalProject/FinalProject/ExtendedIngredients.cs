﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class ExtendedIngredients
    {
        public string aisle { get; set; }
        public long amount { get; set; }
        public string consitency { get; set; }
        public long id { get; set; }
        public string image { get; set; }
        public Measure measures { get; set; }
        public List<string> meta { get; set; }
        public string name { get; set; }
        public string original { get; set; }
        public string originalName { get; set; }
        public string unit { get; set; }

    }
}