﻿using finale_project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class RecipeWeek : ContentPage
    {

        Spoonacular _spoonacular;
        public IList<Recipe> recipes { get; private set; }
        public RecipeWeek()
        {
            InitializeComponent();

            BindingContext = this;
            Init();
        }

        private async void Init()
        {
            List<string> day = new List<string>();
            day.Add("Monday");
            day.Add("Tuesday");
            day.Add("Wednesday");
            day.Add("Thursday");
            day.Add("Friday");
            day.Add("Saturday");
            day.Add("Sunday");

            _spoonacular = new Spoonacular();
            await _spoonacular.search();
            recipes = _spoonacular.recipes;

            List<RecipeInfo> recipeInfo = new List<RecipeInfo>();


            int i = 0;
            
            while (i != 7) 
            {
                recipeInfo.Add(await _spoonacular.get_recipe_info(recipes[i].id));

                mainPage.Children.Add(new Label { Text = "Recipe For " + day[i], TextColor = Color.FromHex("#ff8a0d"), FontSize = 40, FontAttributes = FontAttributes.Bold, Padding = 10 });
                mainPage.Children.Add(new Image { Source = "https://webknox.com/recipeImages/" + recipes[i].image });
                mainPage.Children.Add(new Label { Text = recipes[i].title, FontSize = 20, FontAttributes = FontAttributes.Bold, Padding = 10 });
                mainPage.Children.Add(new Label { Text = recipeInfo[i].instructions, FontSize = 20, Padding = 15 });
                mainPage.Children.Add(new Label { Text = "Ready in " + recipes[i].readyInMinutes + " minutes\n" + recipes[i].servings + " plate(s)", FontSize = 15, FontAttributes = FontAttributes.Italic, Padding = 15 });
                i++;
            }
        }

        void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Recipe selectedItem = e.SelectedItem as Recipe;
        }

        void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            Recipe tappedItem = e.Item as Recipe;
        }
    }
}