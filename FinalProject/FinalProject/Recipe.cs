﻿using System;
using System.Collections.Generic;
using System.Text;

namespace finale_project
{
    public class Recipe
    {
        public long id { get; set; }
        public string title { get; set; }
        public long readyInMinutes { get; set; }
        public long servings { get; set; }
        public string image { get; set; }
        public List<string> imageUrls { get; set; }
    }
}
